const settings = require("./settings");
/**
 * Shorthand for creating Elements.
 * @param {*} tag The tag name of the element.
 * @param {*} [props] Optional props.
 * @param {*} children Child elements or strings
 */
function h(tag, props, ...children) {
  let element = document.createElement(tag);
  if (props) {
    if (props.nodeType || typeof props !== "object") {
      children.unshift(props);
    } else {
      for (let name in props) {
        let value = props[name];
        if (name == "style") {
          Object.assign(element.style, value);
        } else {
          element.setAttribute(name, value);
          element[name] = value;
        }
      }
    }
  }
  for (let child of children) {
    element.appendChild(
      typeof child === "object" ? child : document.createTextNode(child)
    );
  }
  return element;
}

function successMessage() {
  let dialog = document.createElement("dialog");

  // main container
  let container = document.createElement("div");
  container.style.minWidth = 400;
  container.style.padding = 10;
  container.style.textAlign = "center";

  // add image
  let img = document.createElement("img");
  //   img.style = {...img.style, width: 200, height: 200};
  img.src = "images/icon.png";
  img.style.width = 50;
  img.style.height = 50;
  img.style.display = "block";
  img.style.marginBottom = 20;
  container.appendChild(img);

  // add content
  let title = document.createElement("h3");
  title.style.padding = 20;
  title.textContent = `React Component(s) created`;
  container.appendChild(title);

  // close button
  let closeButton = document.createElement("button");
  closeButton.textContent = "OK";
  container.appendChild(closeButton);
  closeButton.onclick = e => {
    dialog.close();
  };

  document.body.appendChild(dialog);
  dialog.appendChild(container);
  dialog.showModal();
}

function askSettings() {
  let dialog = document.createElement("dialog");

  // main container
  let container = document.createElement("div");
  //   container.style.minWidth = 400;
  //   container.style.padding = 10;
  //   container.style.textAlign = "center";

  container.innerHTML = `

        <style>
            form {
                
            }
            .h1 {
                align-items: center;
                justify-content: space-between;
                display: flex;
                flex-direction: row;
            }
            .icon {
                border-radius: 4px;
                width: 24px;
                height: 24px;
                overflow: hidden;
            }

            .row {
                align-items: center;
                justify-content: flex-start;
                display: flex;
                flex-direction: row;
            }
            label {
                margin-bottom: 15px;
                padding-bottom: 15px;
                border-bottom: 1px solid rgba(0,0,0,0.02);
            }
            label .small {
                font-size: 10px;
            }
        </style>
        <form id="settingsForm" name="settingsForm">
            <h1 class="h1">
                <span>React Exporter</span>
                <img class="icon" src="images/icon.png" width="50" height="50" />
            </h1>
            <hr />
            <p>Please select your options for creating React or React native Components</p>

            
            <label >
                <div class="row">
                <input type="checkbox" name="icon" />
                <span>Icon</span>
                
                </div>
                <p class="small">Replace SVG "width" and "height" value by "1em" in order to make SVG size inherits from text size.</p>
                
            </label>

            <label>
                <div class="row">
                <input type="checkbox" name="dimensions" />
                <span>Dimensions</span>
                
                </div>
                <p class="small">Remove width and height from root SVG tag.</p>
            </label>

            <label>
                <span>Expand Props:</span>
                <select name="expandProps">
                    <option value="start" selected="selected">start</option>
                    <option value="end">end</option>
                    <option value="false">false</option>
                </select>
                <p class="small">All properties given to component will be forwarded on SVG tag. Possible values: "start", "end" or false.</p>
             </label>


            <label>
                <div class="row">
                <input type="checkbox" name="ref" />
                <span>Ref</span>
                
                </div>
                <p class="small">Setting this to true will forward ref to the root SVG tag.</p>
            </label>



            <label>
                <div class="row">
                <span>Export for platform:</span>
                <select name="native">
                    <option value="false" selected="selected">React</option>
                    <option value="true">React Native</option>
                </select>
                </div>
                
                <p class="small">Should the export target be React or ReactNative</p>
                
            </label>


            <label>
                <div class="row">
                <input type="checkbox" name="stories" />
                <span>React Storybook</span>
                
                </div>
                <p class="small">Generate valid files for "The UI Development Environment" StoryBook</p>
            </label>

            

            

            


            <footer>
                <button uxp-variant="primary" id="cancel" name="cancel">Cancel</button>
                <button type="submit" uxp-variant="cta" id="ok" name="submit">Create</button>
            </footer>



        </form>
    `;

  //   // add image
  //   let img = document.createElement("img");
  //   //   img.style = {...img.style, width: 200, height: 200};
  //   img.src = "images/icon.png";
  //   img.style.width = 50;
  //   img.style.height = 50;
  //   img.style.display = "block";
  //   img.style.marginBottom = 20;
  //   container.appendChild(img);

  document.body.appendChild(dialog);
  dialog.appendChild(container);

  

  //const settingsForm = document.querySelector("#settingsForm");

  const initialSettings = settings.getSettings().then(initialSettings => {
    const settingsForm = document.querySelector("#settingsForm");
    const settingsFormIcon = document.querySelector("input[name=icon]");
  // TODO check this:  console.log("settingsFormIcon", initialSettings);
  settingsFormIcon.checked = initialSettings.icon;
  
  const settingsFormDimensions = document.querySelector("input[name=dimensions]");
  settingsFormDimensions.checked = initialSettings.dimensions;

  const settingsFormExpandProps = document.querySelector("select[name=expandProps]");
//   settingsFormExpandProps.value = (initialSettings.expandProps == false ? "false" : initialSettings.expandProps);

  const settingsFormRef = document.querySelector("input[name=ref]");
  settingsFormRef.checked = initialSettings.ref;

  const settingsFormPlatform = document.querySelector("select[name=native]");
//   settingsFormPlatform.value = (initialSettings.native == false ? "false" : "true");

  const settingsFormStories = document.querySelector("input[name=stories]");
  settingsFormStories.checked = initialSettings.stories;

  settingsForm.addEventListener("submit", e => {
    // // TODO check this:  console.log(settingsForm.elements);
    e.preventDefault();
    
    
    // // TODO check this:  console.log("settingsForm");
    // // TODO check this:  console.log(formData);
   
  });

  const cancelButton = container.querySelector("#cancel");
  cancelButton.addEventListener("click", () => dialog.close("reasonCanceled"));

  
  const okButton = container.querySelector("#ok");
  okButton.addEventListener("click", e => {
    // onsubmit();
    e.preventDefault();
    const settingsForm = document.querySelector("#settingsForm");
    // settingsForm.submit();
    // document.forms["settingsForm"].submit();

    let newSettings = {
        // Replace SVG "width" and "height" value by "1em" in order to make SVG size inherits from text size.
        // https://github.com/smooth-code/svgr#icon
        icon: true,
  
        // Modify all SVG nodes with uppercase and use a specific template with react-native-svg imports. All unsupported nodes will be removed.
        // https://github.com/smooth-code/svgr#native
        native: true,
  
        // All properties given to component will be forwarded on SVG tag. Possible values: "start", "end" or false.
        // https://github.com/smooth-code/svgr#expand-props
        expandProps: "start",
  
        // Remove width and height from root SVG tag.
        // https://github.com/smooth-code/svgr#dimensions
        dimensions: true,
  
        // Setting this to true will forward ref to the root SVG tag.
        // https://github.com/smooth-code/svgr#ref
        ref: false
      };
      for (let i of settingsForm.elements) {
      //   // TODO check this:  console.log("\n\n#############################");
      //   // TODO check this:  console.log(i.name);
      //   // TODO check this:  console.log(i.getAttribute("name"));
      //   // TODO check this:  console.log(i.nodeName);
      //   // TODO check this:  console.log(i.tagName);
      //   // TODO check this:  console.log(i.localName);
  
      //   // TODO check this:  console.log(i.value);
        switch (i.nodeName) {
          case "INPUT":
          // TODO check this:  console.log(i.name, "::","on:", i.checked , (i.checked == true ? true : false));
            newSettings[i.getAttribute("name")] = i.checked;
            break;
          case "SELECT":
              newSettings[i.getAttribute("name")] = (i.value ? i.value : false);
            break;
          default:
            break;
        }
      }
      // const formData = new FormData(settingsForm);
      settings.setSettings(newSettings).then(()=>{
          // TODO check this:  console.log("settings saved");
          dialog.close("reasonCanceled");
          settings.getSettings().then((s)=>{
  
              // TODO check this:  console.log("getSettings: ", s);
          })
      });
    
  });
  });

  

  //   // cancel button
  //   let cancelButton = document.createElement("button");
  //   cancelButton.textContent = "cancel";
  //   container.appendChild(cancelButton);
  //   cancelButton.onclick = e => {
  //     dialog.close("reasonCanceled");
  //   };

  //   // close button
  //   let closeButton = document.createElement("button");
  //   closeButton.textContent = "OK";
  //   container.appendChild(closeButton);
  //   closeButton.onclick = e => {
  //     dialog.close("run_export");
  //   };

  return dialog; //dialog.showModal();
}

module.exports = {
  successMessage,
  askSettings
};
