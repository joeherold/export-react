const svgr = require("@svgr/core").default;
const jsx = require("@svgr/plugin-jsx").default;
const prettier = require("./prettier");
const prettierBabylon = require("./parsers/babylon");
const _ = require("lodash");


function getComponentName(componentName, isNative) {
  return (componentName =
    getCleanName(
      componentName.charAt(0).toUpperCase() + componentName.slice(1)
    ) + (isNative === true ? "NativeComponent" : "Component"));
}

function getCleanName(name) {
  // fixed iussue #2
  return _.upperFirst(_.camelCase(name.replace(/ /g, "").replace(/[^a-zA-Z]+/g, "-")));
}

async function createReactComponent(
  inFile,
  outFile,
  componentName,
  opts,
  storageFolder,
  doStories

) {
  try {
    const svgCode = await inFile.read();
    const plugins = [
      jsx,
      function (data, config, state) {
        data = `/**
         *
         * This file was generated with Adobe XD React Exporter
         * Exporter for Adobe XD is written by: Johannes Pichler <j.pichler@webpixels.at>
         * 
         **/

         
        ${data}
        `;

        data = data.replace("/* SVGR", "/* Adobe XD React Exporter");

        return prettier.format(data, {
          parser: "babylon",
          plugins: [prettierBabylon]
        });
      }
    ];

    const jsCode = await svgr(
      svgCode, {
        // Replace SVG "width" and "height" value by "1em" in order to make SVG size inherits from text size.
        // https://github.com/smooth-code/svgr#icon
        icon: true,

        // Modify all SVG nodes with uppercase and use a specific template with react-native-svg imports. All unsupported nodes will be removed.
        // https://github.com/smooth-code/svgr#native
        native: true,

        // All properties given to component will be forwarded on SVG tag. Possible values: "start", "end" or false.
        // https://github.com/smooth-code/svgr#expand-props
        expandProps: "start",

        // Remove width and height from root SVG tag.
        // https://github.com/smooth-code/svgr#dimensions
        dimensions: true,

        // Setting this to true will forward ref to the root SVG tag.
        // https://github.com/smooth-code/svgr#ref
        ref: false,

        // destructuring parent options
        ...opts,

        // overwrite plugins, to ensure only working ones
        plugins: plugins
      }, {
        componentName: componentName
      }
    );


    if (doStories === true) {
      const stripedComponentName = componentName.replace("Component", "");
      const story = `/**
    *
    * This file was generated with Adobe XD React Exporter
    * Exporter for Adobe XD is written by: Johannes Pichler <j.pichler@webpixels.at>
    * 
    **/
    
    import React from 'react';
    import { storiesOf } from '@storybook/react';
    import { action } from '@storybook/addon-actions';
    
    import ${stripedComponentName} from './${componentName}';
    
    export const myProps = {
      id: '1',
      className: 'mySampleClass',
      sampleString: 'Test Task',
      sampleDate: new Date(2018, 0, 1, 9, 0)
    };
    
    export const actions = {
      onActionOne: action('onActionOne'),
      onActionTwo: action('onActionTwo')
    };
    
    storiesOf('${stripedComponentName}', module)
      .add('default', () => <${stripedComponentName}  {...myProps } {...actions} />)
      .add('pinned', () => <${stripedComponentName} {...myProps } pinned={true} {...actions} />)
      .add('archived', () => <${stripedComponentName} { ...myProps} archived={true} {...actions} />);
    `;

      const storyFile = await storageFolder.createEntry(
        componentName + ".stories.js", {
          overwrite: true
        }
      );
      await storyFile.write(
        prettier.format(story, {
          parser: "babylon",
          plugins: [prettierBabylon]
        })
      );

    }

    await outFile.write(jsCode);
    try {
      await inFile.delete();
    } catch (e) {
    }
    return true;
  } catch (e) {
    console.error(e);
    return false;
  }
}

module.exports = {
  createReactComponent,
  getComponentName,
  getCleanName
};