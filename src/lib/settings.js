const uxp = require("uxp");

async function getSettings() {
  const fs = uxp.storage.localFileSystem;
  const pluginDataFolder = await fs.getDataFolder();
  // TODO check this:  console.log("pluginDataFolder", pluginDataFolder.nativePath);
  
  let settingsFile;
  try {
   settingsFile = await pluginDataFolder.getEntry("settings.json");
   const settings = await settingsFile.read();
   
   let returner = JSON.parse(settings);

   if (returner.expanProps && returner.expanProps == "false") {
    returner.expanProps = false;
  }
  
  return returner;

  } catch(e) {

    settingsFile = await pluginDataFolder.createEntry("settings.json", {
        overwrite: true
      });
      try {
        // const settingsFile = await pluginDataFolder.createEntry("settings.json", {overwrite: true});
        const settings = {
          // Replace SVG "width" and "height" value by "1em" in order to make SVG size inherits from text size.
          // https://github.com/smooth-code/svgr#icon
          icon: false,
  
          // Modify all SVG nodes with uppercase and use a specific template with react-native-svg imports. All unsupported nodes will be removed.
          // https://github.com/smooth-code/svgr#native
          native: false,
  
          // All properties given to component will be forwarded on SVG tag. Possible values: "start", "end" or false.
          // https://github.com/smooth-code/svgr#expand-props
          expandProps: "false",
  
          // Remove width and height from root SVG tag.
          // https://github.com/smooth-code/svgr#dimensions
          dimensions: true,
  
          // Setting this to true will forward ref to the root SVG tag.
          // https://github.com/smooth-code/svgr#ref
          ref: false,

          stories: true,
        };
  
        const strSettings = JSON.stringify(settings);
  
        await settingsFile.write(strSettings);
  
        return settings;
      } catch (e) {
        return {};
      }

  }
}

async function setSettings(newSetetings) {
  const fs = uxp.storage.localFileSystem;
  const pluginDataFolder = await fs.getDataFolder();

  if (newSetetings.expanProps && newSetetings.expanProps == "false") {
    newSetetings.expanProps = false;
  }

  

  let settings = await getSettings();

  const veryNewSettings = Object.assign({}, {...settings}, {...newSetetings}); //  {{}, {}, {...newSettings}};
  
  
  const settingsFile = await pluginDataFolder.getEntry("settings.json");

  if (await settingsFile.read()) {
    await settingsFile.write(JSON.stringify(veryNewSettings));
    return true;
  } else {
    return true;
  }
}

module.exports = {
  setSettings,
  getSettings
};
