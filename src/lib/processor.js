const application = require("application");
const uxp = require("uxp");
const path = require("path");
const reactWorker = require("./reactCreator");
const {
    error,
    alert
} = require("@adobe/xd-plugin-toolkit").dialogs;

async function processReactExport(selection, opts) {


    // verify, that all values are in a propper shape
    opts = prepareOpts(opts);
    const doStories = opts.stories;
    delete opts.stories;


    const fs = uxp.storage.localFileSystem;

    // check if elements are in selection stack
    if (selection.items.length > 0) {

        // ask for the export folder
        const folder = await fs.getFolder();
        let arrFiles = [];

        // create a renditions array
        let renditions = [];

        // create index for async iteration
        let index = 0;


        for (const item of selection.items) {

            // create a file by async function (shorthand for promise)
            let inFile = await folder.createFile(
                "ComponentTMP_" + index + ".svg", {
                    overwrite: true
                }
            );


            // create an outfile js object in filestorage
            let outFile = await folder.createFile(
                reactWorker.getComponentName(item.name) + ".js", {
                    overwrite: true
                }
            );

            // push both files to array stack
            arrFiles.push({
                inFile: inFile,
                outFile: outFile
            });

            // push settings for the renditions stack
            // Link: https://adobexdplatform.com/plugin-docs/tutorials/how-to-export-a-rendition/#4-define-your-rendition-settings
            const renditionSetting = {
                node: item,
                outputFile: arrFiles[index].inFile,
                type: "svg",
                scale: 1,
                minify: true,
                embedImages: false
            };
            renditions.push(renditionSetting);
            index++;
        }







        // start creating the renditions
        // Link: https://adobexdplatform.com/plugin-docs/tutorials/how-to-export-a-rendition/
        const renditionResults = await application.createRenditions(renditions);
        let indexTwo = 0;
        let totalSuccess = true;
        for (const item of selection.items) {
            const success = await reactWorker.createReactComponent(
                arrFiles[indexTwo].inFile,
                arrFiles[indexTwo].outFile,
                reactWorker.getComponentName(item.name), opts,
                folder,
                doStories
            );
            if (success !== true) {
                totalSuccess = false;
            }

            indexTwo++;
        }

        return totalSuccess;




    } else {
        await error("ERROR", ["No elements selected", "You have to select at least one Element or Group"]);
        return false;
    }
}

function prepareOpts(inputOpts) {

    let opts = { ...inputOpts };

    if (opts.expandProps === "false") opts.expandProps = false;
    if (opts.stories === true) {
        opts.stories = true;
    } else {
        opts.storeies = false;
    }

    return opts;
}

export default processReactExport;