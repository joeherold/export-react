const reactShim = require("./react-shim");
const style = require("./styles.css");
const React = require("react");
const ReactDOM = require("react-dom");

// components
const App = require("./App").default;
const appStyles = {
  margin: 0,
  padding: 0
};

// dialog
let dialog;
function getDialog(selection) {
  if (dialog == null) {
    dialog = document.createElement("dialog");
    dialog.style.padding = 0;
    dialog.style.margin = 0;
    ReactDOM.render(
      <App dialog={dialog} selection={selection} style={appStyles} />,
      dialog
    );
  }
  return dialog;
}

// export var
const exporter = {
  commands: {
    menuCommand: function (selection) {
      document.body.appendChild(getDialog(selection)).showModal();
    }
  }
};

// export
module.exports = exporter;
