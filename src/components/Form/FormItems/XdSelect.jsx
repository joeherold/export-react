const React = require('react');
// const PropTypes = require('prop-types');

// This component exists because <select>s' default values don't work in XD.
class XdSelect extends React.Component {
    constructor() {
        super(...arguments);
        this.selectRef = React.createRef();
    }

    componentDidMount() { this._selectOptionByValue(); }
    componentDidUpdate() { this._selectOptionByValue(); }

    _selectOptionByValue() {
        const selectEl = this.selectRef.current;
        const {options, value} = this.props;
        selectEl.selectedIndex = options.findIndex(option => (
          option.value === value
        ))
    }

    render() {
        const {options, disabled, value, onChange} = this.props;
        return (
            <select ref={this.selectRef} value={value || ''} onChange={onChange} disabled={Boolean(disabled)}>
                {options.map(({value, name}) => (
                    <option key={value} value={value}>{name}</option>
                ))}
            </select>
        );
    }
}

// XdSelect.propTypes = {
//     options: PropTypes.arrayOf(PropTypes.shape({
//         value: PropTypes.string.isRequired,
//         name: PropTypes.string.isRequired
//     })).isRequired,
//     value: PropTypes.string.isRequired,
//     onChange: PropTypes.func.isRequired,
//     disabled: PropTypes.bool,
// };

module.exports = XdSelect;