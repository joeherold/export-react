import React from "react";
import {
  labelStyle,
  formRowStyle,
  inputRowStyle,
  descriptionStyle
} from "./styles/formStyles";

const FormItemRadio = ({
  stateKey,
  value,
  options,
  changeHandler,
  title,
  description
}) => {
  return (
    <div style={formRowStyle}>
      <div style={inputRowStyle}>
        <label style={labelStyle}>{title}</label>
        <select
          name={stateKey}
          onChange={changeHandler}
          value={value}
          defaultValue={value}
        >
          {options.map(item => (
            <option key={stateKey + "_" + item} name={stateKey} value={item}>
              {item === false ? "false" : item}
            </option>
          ))}
        </select>
      </div>

      <p style={descriptionStyle}>{description}</p>
    </div>
  );
};

export default FormItemRadio;
