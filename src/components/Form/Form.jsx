import React from "react";

import FormItemCheckbox from "./FormItemCheckbox";
import FormItemSelect from "./FormItemSelect";
import FormItemRadio from "./FormItemRadio";

const doit = e => handleInputChange(e);

const Form = ({ state, handleInputChange, handleExpandPropsChange }) => {
  const propsStories = {
    stateKey: "stories",
    value: state.stories,
    changeHandler: handleInputChange,
    title: "create React Storybook Files",
    description: `Should React Storybooks files be created along with the component files?`
  };

  const propsIcon = {
    stateKey: "icon",
    value: state.icon,
    changeHandler: handleInputChange,
    title: "use as Icon",
    description: `Replace SVG "width" and "height" value by "1em" in order to make SVG size inherits from text size.`
  };

  const propsNative = {
    stateKey: "native",
    value: state.native,
    // options: ["a", "b", "true", "false"],
    changeHandler: handleInputChange,
    title: "create for React Native",
    description: `Modify all SVG nodes with uppercase and use a specific template with react-native-svg imports. All unsupported nodes will be removed.`
  };

  const propsExpandProps = {
    stateKey: "expandProps",
    value: state.expandProps,
    options: [
      { value: "start", name: "start" },
      { value: "end", name: "end" },
      { value: "false", name: "false" }
    ],
    changeHandler: handleExpandPropsChange,
    title: "Expand Props",
    description: `All properties given to component will be forwarded on SVG tag. Possible values: "start", "end" or false.`
  };

  const propsDimensions = {
    stateKey: "dimensions",
    value: state.dimensions,
    changeHandler: handleInputChange,
    title: "remove Dimensions",
    description: `Remove width and height from root SVG tag.`
  };

  const propsRef = {
    stateKey: "ref",
    value: state.ref,
    changeHandler: handleInputChange,
    title: "forward Ref",
    description: `Setting this to true will forward ref to the root SVG tag.`
  };

  return (
    <form onSubmit={e => {}}>
      <FormItemCheckbox {...propsIcon} />
      <FormItemCheckbox {...propsNative} />
      <FormItemSelect {...propsExpandProps} />
      <FormItemCheckbox {...propsDimensions} />
      <FormItemCheckbox {...propsRef} />
      <FormItemCheckbox {...propsStories} />
    </form>
  );
};

export default Form;
