import React from "react";
import {
  labelStyle,
  formRowStyle,
  inputRowStyle,
  descriptionStyle
} from "./styles/formStyles";
import XdSelect from "./FormItems/XdSelect";

const FormItemSelect = ({
  stateKey,
  value,
  options,
  changeHandler,
  title,
  description
}) => {
  return (
    <div style={formRowStyle}>
      <div style={inputRowStyle}>
        <label style={labelStyle}>{title} </label>
        <XdSelect
          options={options}
          value={value}
          onChange={changeHandler}
          disabled={false}
        />
      </div>

      <p style={descriptionStyle}>{description}</p>
    </div>
  );
};

export default FormItemSelect;
