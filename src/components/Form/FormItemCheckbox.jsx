import React from "react";
import {
  labelStyle,
  formRowStyle,
  inputRowStyle,
  descriptionStyle
} from "./styles/formStyles";
const FormItemCheckbox = ({
  stateKey,
  value,
  changeHandler,
  title,
  description
}) => {
  return (
    <div style={formRowStyle}>
      <div style={inputRowStyle}>
        <label style={labelStyle}>{title}</label>
        <input
          name={stateKey}
          type="checkbox"
          defaultChecked={value}
          // onChange={() => {}}
          ref={el => el && el.addEventListener("change", changeHandler)}
        />
      </div>

      <p style={descriptionStyle}>{description}</p>
    </div>
  );
};

export default FormItemCheckbox;
