export const labelStyle = {
    display: "block",
    order: 2,
    
    width: 400,
    flexShrink: 0,
    flexGrow: 0,
    margin: 0,
    padding: 0,
    marginRight: 10
};
export const formRowStyle = {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    width: "100%",
    flexBase: "100%",
    //   border: "1px solid red"
    // paddingBottom: 12,
    marginBottom: 20,
    // borderBottom: "1px solid rgba(0,0,0,0.0)"
};

export const inputRowStyle = {
    display: "flex",
    flexShrink: 0,
    flexGrow: 0,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    alignContent: "center"
};


export const descriptionStyle = {

    color: "rgba(0,0,0,0.5)",
    fontSize: "10px",
    display: "inline-block",
    margin: 0,
    padding: 0,
    paddingLeft: 22,
    whiteSpace: "normal",
    width: "100%",
    boxSizing: "border-box",
    lineHeight: "12px",
    // border: "1px solid red"
  }