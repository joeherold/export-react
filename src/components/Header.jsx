import React from "react";

const imgStyles = {
  width: 50,
  height: 50,
  marginBottom: 10
};
const headerStyles = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  alignContent: "flex-end",
  alitnItems: "flex-end",
  borderBottom: "1px solid rgba(0,0,0,0.1)",
  marginBottom: 25
};
const versionStyle = {
  display: "block",
  fontSize: 8,
  fontWeight: "normal"
};
export default ({ icon, nativ }) => {
  return (
    <dev style={headerStyles}>
      <div>
        <h1>React Exporter</h1>
        <a href="https://webpixels.at" target="_blank" style={versionStyle}>
          by webpixels.at
        </a>
        <a
          href="https://github.com/smooth-code/svgr"
          target="_blank"
          style={versionStyle}
        >
          Processing based on SVGR
        </a>
      </div>
      <img id="header-img" src="images/icon_192.png" style={imgStyles} />
    </dev>
  );
};
