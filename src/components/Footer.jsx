import React, { Component } from "react";

class Footer extends Component {
  constructor(props) {
    super(props);

    this.style = {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      marginTop: 10,
      marginBottom: 10
    };

    this.buttonCancelClick = this.buttonCancelClick.bind(this);
    this.buttonOkClick = this.buttonOkClick.bind(this);
  }

  buttonCancelClick() {
    // // TODO check this:  console.log(this.props);
    this.props.dialog.close();
  }

  buttonOkClick(e) {
    e.preventDefault();
    this.props.actionGenerate();
  }

  render() {
    return (
      <footer style={this.style}>
        <button
          uxp-variant="primary"
          id="cancel"
          name="cancel"
          onClick={this.buttonCancelClick}
        >
          Cancel
        </button>
        <button
          type="submit"
          uxp-variant="cta"
          id="ok"
          name="submit"
          onClick={this.buttonOkClick}
        >
          Select Folder and Create
        </button>
      </footer>
    );
  }
}

export default Footer;
