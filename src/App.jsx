/**
 * THERE ARE A LOT OF ISSUES WITH XD JS/HTML ENGINE AND REACT.JS
 * DEVELOPERS, SEARCH HERE: https://forums.adobexdplatform.com/search?q=react
 *
 * LIST OF KNOWN ISSUES:
 * Link: https://forums.adobexdplatform.com/t/react-known-issues-and-workarounds/32
 *
 */

import React, { Component } from "react";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Form from "./components/Form/Form";
import CodePreview from "./components/CodePreview";
import SettingsHandler from "./lib/settings";
import Processor from "./lib/processor";
const { error, alert } = require("@adobe/xd-plugin-toolkit").dialogs;
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // Replace SVG "width" and "height" value by "1em" in order to make SVG size inherits from text size.
      // https://github.com/smooth-code/svgr#icon
      icon: true,

      // Modify all SVG nodes with uppercase and use a specific template with react-native-svg imports. All unsupported nodes will be removed.
      // https://github.com/smooth-code/svgr#native
      native: false,

      // All properties given to component will be forwarded on SVG tag. Possible values: "start", "end" or false.
      // https://github.com/smooth-code/svgr#expand-props
      expandProps: "start",

      // Remove width and height from root SVG tag.
      // https://github.com/smooth-code/svgr#dimensions
      dimensions: true,

      // Setting this to true will forward ref to the root SVG tag.
      // https://github.com/smooth-code/svgr#ref
      ref: false,

      // should React Storybook files be generated?
      stories: true
    };

    this.style = {
      width: 450,
      margin: 0,
      padding: "10px 20px 10px 20px",
      overflow: "hidden"
    };

    this.height = null;

    this.rowStyle = {
      display: "flex",
      flexDirection: "row",
      height: this.height
    };

    // this.updateAppState = this.updateAppState.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleExpandPropsChange = this.handleExpandPropsChange.bind(this);
    this.generateExport = this.generateExport.bind(this);
  }


  async componentDidMount() {
    // this.dialog.onsubmit = function () {
    //   this.generateExport();
    // }
  }
  async componentWillMount() {
    const settings = await SettingsHandler.getSettings();
    this.setState(settings);

    // console.log("generateExport Selection: ", this.props.selection, this.props.selection.items.length);

  }



  async generateExport() {

    if (this.props.selection && this.props.selection.items.length > 0) {

      const success = await Processor(this.props.selection, this.state);
      if (success === true) {
        this.props.dialog.close("success");
        await alert("Finished", [
          "The export or your selected elements has finished with success"
        ]);
      } else {
        this.props.dialog.close("error");
        await error("ERROR", [
          "During the export or your selected elements errors occured."
        ]);
      }


    } else {

      this.props.dialog.close("error");
      await error("ERROR: nothing to export...", [
        "You must select at least one Element, Layer, Group or Artboard! You did not select anything, so nothing can be exported."
      ]);
    }

  }

  async handleInputChange(event) {
    const target = event.target;
    let value = target.type === "checkbox" ? !!target.checked : target.value;
    const name = target.getAttribute("name");

    await this.setState({
      [name]: value
    });

    await SettingsHandler.setSettings(this.state);
  }

  async handleExpandPropsChange(e) {
    const name = "expandProps";
    const value = e.target.value;

    await this.setState({
      expandProps: e.target.value
    });

    await SettingsHandler.setSettings(this.state);
  }

  handleFormSubmit(event) {
    this.generateExport();
  }

  render() {
    return (
      <div style={this.rowStyle}>
        <div style={this.style}>
          <Header dialog={this.props.dialog} state={this.state} />
          <Form
            state={this.state}
            handleInputChange={this.handleInputChange}
            handleExpandPropsChange={this.handleExpandPropsChange}
            handleFormSubmit={this.handleFormSubmit}
          />

          <Footer
            dialog={this.props.dialog}
            actionGenerate={this.generateExport}
            state={this.state}
          />
        </div>
        {/* <CodePreview  height={this.height} /> */}
      </div>
    );
  }
}

export default App;
